import * as firebase from 'firebase'
import firebaseConfig from './config'

// Initialize Firebase
if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig);
}
else{
    firebase.app();
}

const Firebase = {
    loginWithEmail: (email, password) => {
        let response = {}
        return new Promise(async(resolve,reject)=>{
            try{
                let user_data = await firebase.auth().signInWithEmailAndPassword(email, password)
                response.status = true;
                response.message = "Login Success";
                response.data = user_data;
                resolve(response)
            }
            catch(err){
                response.status = false;
                response.message = err.message;
                response.data = undefined;
                resolve(response)
            }
        })
    },
    
    signupWithEmail: (email, password) => {
        let response = {}
        return new Promise(async(resolve,reject)=>{
            try{
                let user_data = await firebase.auth().createUserWithEmailAndPassword(email, password)
                response.status = true;
                response.message = "Account Created Successfully";
                response.data = user_data;
                resolve(response)
            }
            catch(err){
                response.status = false;
                response.message = err.message;
                response.data = undefined;
                resolve(err) 
            }
        })
    },
    // signOut: () => {
    //     return firebase.auth().signOut()
    // },
    // checkUserAuth : () => {
    //     return firebase.auth().currentUser;
    // },
    passwordReset : (email) => {
        let response = {}
        return new Promise(async(resolve,reject)=>{
            try{
                let result = await firebase.auth().sendPasswordResetEmail(email)
                response.status = true;
                response.message = "Password Reset Link has been sent to your Registered Email Address.";
                response.data = result;
                resolve(response) 
            }
            catch(err){
                response.status = true;
                response.message = err.message;
                response.data = result;
                reject(response)
            }
        })
    },
//   // firestore
//   createNewUser: userData => {
//     return firebase
//       .firestore()
//       .collection('users')
//       .doc(`${userData.uid}`)
//       .set(userData)
//   }
}

export default Firebase
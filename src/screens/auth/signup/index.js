import React, { Component } from 'react'
import { Text, TextInput, Button, StyleSheet, View, Image, TouchableOpacity, ScrollView, AsyncStorage,ImageBackground } from 'react-native'
import Firebase from "../../../config/firebase/firebase";
import {responsiveWidth, responsiveHeight, responsiveFontSize} from "react-native-responsive-dimensions"
import { SafeAreaView } from "react-native-safe-area-context"
import {Item,Icon,Input} from "native-base"
import DatePicker from 'react-native-datepicker'

export default class SignupScreen extends Component {
  state = {
    fname: '',
    lname: '',
    email: '',
    pass: '',
    conf_pass: '',
    loading: false,
    submitError : "",
    date:new Date()
  }

  goToLogin = () => this.props.navigation.navigate('login')

  validateEmail = ()=>{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/;
    if(emailPattern.test(this.state.email)){
        return true;
    }
    return false;
  }

  handleSubmit = async () => {
    const { fname, lname, email, pass, conf_pass } = this.state
    if(fname == "" || lname == "" || email == "" || pass == "" || conf_pass == ""){
      this.setState({submitError : "Please Fill All the Required Fields"});
      return false;
    }
    if(!this.validateEmail()) {
      this.setState({submitError:"Please Enter Valid Email Address"})
      return false;
    }
    if(this.state.pass !== this.state.conf_pass){
      this.setState({submitError:"Password and Confirm Password Should Be Same"})
      return false;
    }
    this.setState({submitError:"",loading:true})
    try {
      const response = await Firebase.signupWithEmail(email,pass)
      console.log(response);
      this.setState({submitError:response.message,loading:false})
      this.props.navigation.navigate("login")
    } catch (error) {
      this.setState({submitError:error,message,loading:false})
    }
  }

  render() {
    return (
      <SafeAreaView >
          <ImageBackground
            source = {require("../../../assets/background.jpg")}
            style={{width:"100%", height:"100%"}}>
              <Image
                  source = {require("../../../assets/gear.png")}
                  style={{width:"50%", height:'30%', alignSelf:'center',marginTop:25}}
                  resizeMode ="contain"
              /> 
              <ScrollView>
                <Text style={{color:'white', fontSize:responsiveFontSize(3), marginLeft:20}}>SignUp</Text>
                <View style = {styles.afterLogo}>
                    <Text style={styles.errorText}>{this.state.submitError}</Text>
                    <View style={{flexDirection:"row", justifyContent:'space-around'}}>
                        <Item style={{width:170, height:40}}>
                            <Icon active name='user' type='FontAwesome5' style={{color:'white', fontSize:17}} />
                            <Input
                              onChangeText={(fname)=>this.setState({fname})}
                              style={{color:"white"}}
                              placeholder='First name'
                              placeholderTextColor="white"
                            />
                        </Item>

                        <Item  style={{width:170, height:40}}>
                            <Icon active name='user' type='FontAwesome5' style={{color:'white', fontSize:17}} />
                            <Input
                              onChangeText={(lname)=>this.setState({lname})}
                              style={{color:"white"}}
                              placeholder='Last name'
                              placeholderTextColor="white"
                            />
                        </Item>
                    </View>

                        <Item  style={{width:365, height:40, alignSelf:'center', marginTop:20}}>
                            <Icon active name='email' type='Zocial' style={{color:'white', fontSize:17}} />
                            <Input
                              onChangeText={(email)=>this.setState({email})}
                              style={{color:"white"}}
                              placeholder='Email Address'
                              placeholderTextColor="white"
                              autoCapitalize = "none"
                            />
                        </Item>
  
                        <Item style={{width:365, height:40, alignSelf:'center', marginTop:20}}>
                            <Icon active name='key' type='FontAwesome5' style={{color:'white', fontSize:17}} />
                            <Input
                              onChangeText={(pass)=>this.setState({pass})}
                              style={{color:"white"}}
                              placeholder='New password'
                              placeholderTextColor="white"
                              secureTextEntry={true}
                            />
                        </Item>

                        <Item style={{width:365, height:40, alignSelf:'center', marginTop:20}}>
                            <Icon active name='key' type='FontAwesome5' style={{color:'white', fontSize:17}} />
                            <Input
                              onChangeText={(conf_pass)=>this.setState({conf_pass})}
                              style={{color:"white"}}
                              placeholder='Confirm password'
                              placeholderTextColor="white"
                              secureTextEntry={true}
                            />
                        </Item>

                        <View style={{flexDirection:'row', marginTop:20, justifyContent:'space-around'}}>
                        <Text style={{color:'white', fontSize:17, marginTop:7}}>Date of Birth :</Text>
                        <DatePicker
                          style={{width: 200}}
                          date={this.state.date}
                          mode="date"
                          placeholder="select date"
                          format="DD-MMM-YYYY"
                          minDate="2000-05-01"
                          maxDate={this.state.date}
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          customStyles={{
                          dateIcon: {
                          position: 'absolute',
                          left: 0,
                          top: 4,
                          marginLeft: 0
                          },
                          dateInput: {
                          marginLeft: 36
                          }
                          }}
                          onDateChange={(date) => {this.setState({date: date})}}
                        />
                        </View>

                  <View style={styles.buttonContainer}>
                    <TouchableOpacity
                      onPress={()=>this.handleSubmit()}
                      disabled={this.state.loading}
                      style={{backgroundColor:'white',alignItems:'center',alignSelf:'center',height:40,borderRadius:30,width:320,justifyContent:'center'}}>
                      <Text style={{color:'#55add5',fontSize:responsiveFontSize(2.5)}}>{this.state.loading?"Please Wait...":"Submit"}</Text>
                    </TouchableOpacity>
                  </View>

                    <Text style={{color:'white', alignSelf:'center', marginTop:20}}>Have an account?</Text>
                      <TouchableOpacity 
                        onPress={()=>this.goToLogin()}
                        style={{width:80, alignSelf:'center'}}>
                        <Text style={{color:'white', alignSelf:'center', fontSize:responsiveFontSize(1.7)}}>Login</Text>
                      </TouchableOpacity>
                </View>
              </ScrollView>
          </ImageBackground>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 50,
  },
  buttonContainer: {
    marginTop: 20
  },
  feild : {
    width:responsiveWidth(90),
    alignSelf:"center",
    borderBottomColor:'#000',
    borderBottomWidth:0.5,
    fontSize:14,
    paddingVertical: 15,
  },
  logo: {
    height:responsiveHeight(25),
    alignSelf:"center",
    marginVertical:20,
    flex:1
  },
  afterLogo : {
    paddingVertical:0,
    flex:1,
    marginTop:-10
  },
  errorText: {
    color:"white",
    fontSize : 12,
    marginVertical:10,
    alignSelf : "center",
    width : responsiveWidth(90),
    textAlign:"center"
  }
})
import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Image, TextInput, Button, TouchableOpacity, ImageBackground } from 'react-native';
import {responsiveWidth, responsiveHeight, responsiveFontSize} from "react-native-responsive-dimensions"
import {SafeAreaView} from "react-native-safe-area-context"
import Firebase from "../../../config/firebase/firebase"
import {Icon,Item,Input} from "native-base";

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email : "",
      emailerror : "",
      loading : false
    };
  }

  validateEmail = ()=>{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/;
    if(emailPattern.test(this.state.email)){
        return true;
    }
    return false;
  }
  
  resetPassword = () =>{
    let email = this.state.email;
    if(!this.validateEmail()){
      this.setState({emailerror : "Please Enter Valid Email Address"});
      return false;
    }
    this.setState({emailerror : "",loading:true});
    Firebase.passwordReset(email)
    .then((response)=>{
      this.setState({emailerror : response.message})
      setTimeout(()=>{
        this.setState({emailerror : "",loading:false})
      },2000);
    })
    .catch((err)=>{
      this.setState({emailerror : err.message,loading:false})
    })
  }

  goToLogin = () =>{
    this.props.navigation.navigate("login");
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
          <ImageBackground
              source = {require("../../../assets/background.jpg")}
              style={{width:"100%", height:"100%"}}>
                <Image
                  source = {require("../../../assets/gear.png")}
                  style={{width:"50%", height:'30%', alignSelf:'center',marginTop:25}}
                  resizeMode ="contain"
                /> 
                <ScrollView>
                 
                  <View style={styles.afterLogo}>
                  <Text style={{color:'white',fontSize:responsiveFontSize(3), marginLeft:40}}>Forgot Password?</Text>
                        <Item rounded style={{width:320, height:40, alignSelf:'center',marginTop:30}}>
                          <Icon active name='user' type='FontAwesome5' style={{color:'white', fontSize:17}} />
                          <Input
                            onChangeText={(email) => this.setState({email})}
                            style={{color:"white"}}
                            placeholder='Enter your Registered Email'
                            placeholderTextColor="white"
                            autoCapitalize="none"
                          />
                        </Item>
                        
                      <View>
                        <Text style={styles.errorText}>{this.state.emailerror}</Text>
                      </View>

                      <View style={styles.btnView}>
                        <TouchableOpacity
                          onPress={() => this.resetPassword()}
                          disabled={this.state.loading}
                          style={{backgroundColor:'white',alignItems:'center',alignSelf:'center',height:40,borderRadius:30,width:320,justifyContent:'center'}}>
                          <Text style={{color:'#55add5',fontSize:responsiveFontSize(2.5)}}>{this.state.loading?"Please Wait...":"Send Password Reset Link"}</Text>
                        </TouchableOpacity>

                          <Text style={{color:'white', alignSelf:'center', marginTop:40}}>Remember Password?</Text>
                          <TouchableOpacity 
                            onPress={this.goToLogin}
                            style={{width:80, alignSelf:'center'}}>
                            <Text style={{color:'white', alignSelf:'center', fontSize:responsiveFontSize(1.7)}}>Sign In</Text>
                          </TouchableOpacity>
                      </View>
                  </View>
                </ScrollView>
          </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  afterLogo : {
    paddingVertical:10,
    flex:1,
    marginTop:80
  },
  feild : {
    width:responsiveWidth(90),
    alignSelf:"center",
    borderBottomColor:'#000',
    borderBottomWidth:0.5,
    fontSize:14
  },
  errorText: {
    color:"white",
    fontSize : 12,
    marginLeft:responsiveWidth(5),
    marginVertical:10
  },
  btnView:{
    width:responsiveWidth(90),
    alignSelf:"center",
    marginTop : 10,
  }
});

export default ForgotPassword;
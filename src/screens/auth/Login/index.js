import React, { Component } from 'react'
import { ScrollView, Button, TextInput, Text, StyleSheet, View, Image, AsyncStorage, TouchableOpacity,ImageBackground } from 'react-native'
import { responsiveWidth, responsiveHeight,responsiveFontSize } from "react-native-responsive-dimensions";
import Firebase from "../../../config/firebase/firebase";
import {Icon,Item,Input} from "native-base";
import { SafeAreaView } from "react-native-safe-area-context"
export default class LoginClass extends Component {
  state = {
    passwordVisibility: true,
    email : '',
    pass : '',
    emailerror : '',
    passerror : '',
    callerror : '',
    loading : false
  }

  goToSignup = () => this.props.navigation.navigate('signup')
  goToForgotPassword = () => this.props.navigation.navigate('forgotpassword')

  validateEmail = ()=>{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/;
    if(emailPattern.test(this.state.email)){
        return true;
    }
    return false;
  }

  handlePasswordVisibility = () => {
    this.setState({
      rightIcon: this.state.rightIcon === 'eye' ? 'eye-slash' : 'eye',
      passwordVisibility: !this.state.passwordVisibility
    })
  }

  handleOnLogin = async () => {
    this.setState({callerror : ""})
    if(!this.validateEmail()){
      this.setState({emailerror:"Please Enter Valid Email Address"})
      return false;
    }
    this.setState({emailerror:""})
    if(this.state.pass === ""){
      this.setState({passerror:"Please Enter Password"})
      return false;
    }
    this.setState({passerror:"",loading:true});
    try {
      const response = await Firebase.loginWithEmail(this.state.email, this.state.pass)
      if (response.status) {
        this.storeItem("UID",response.data.user.uid);
        this.storeItem("Password",this.state.pass);
        this.storeItem("Email", this.state.email);
        this.setState({loading:false});
        this.props.navigation.navigate('Garages');
        return
      }
      this.setState({loading:false, callerror:response.message});
    } catch (error) {
      this.setState({loading:false, callerror:error.message})
    }
  }

  storeItem = async (key, item) => {
    return new Promise(async (resolve, reject) => {
      try {
        var jsonOfItem = await AsyncStorage.setItem(key, JSON.stringify(item));
        resolve(jsonOfItem);
      } catch (error) {
        reject(error);
      }
    })
  } 

  render() {
    return (
      <SafeAreaView style={styles.container}>
      
        <ImageBackground
            source = {require("../../../assets/background.jpg")}
            style={styles.logo}
           // resizeMode ="contain"
          >
            
             <Image
            source = {require("../../../assets/gear.png")}
            style={{width:"50%", height:'30%', alignSelf:'center',marginTop:25}}
            resizeMode ="contain"
          /> 
          
        <ScrollView>
          <Text style={{color:'white', fontSize:responsiveFontSize(3), marginLeft:40}}>
            Login
          </Text>
        
        <View style={styles.usern}>
            <Item rounded style={{width:320, height:40}}>
             <Icon active name='user' type='FontAwesome5' style={{color:'white', fontSize:17}} />
             <Input
              onChangeText={(email) => this.setState({email})}
              autoCapitalize = "none"
              style={{color:"white"}}
              placeholder='Enter email'
              placeholderTextColor="white"
             />
            </Item>
            <View>
              <Text style={styles.errorText}>{this.state.emailerror}</Text>
            </View>

           <Item rounded style={{ width:320, height:40, marginTop:10}}>
             <Icon active name='key' type='FontAwesome5' style={{color:'white', fontSize:17}} />
             <Input 
              onChangeText={(pass)=>this.setState({pass})}
              secureTextEntry={this.state.passwordVisibility}
              style={{color:"white"}}
              placeholder='Password'
              placeholderTextColor="white"
             />
            </Item>
            <View>
              <Text style={[styles.errorText,{marginTop:10}]}>{this.state.passerror}</Text>
            </View>
        </View>
        
        <TouchableOpacity 
            style={{height:20, width:130,marginLeft:40, marginTop:10}}
            onPress={this.goToForgotPassword}>
            <Text style={{color:"white",width:130 }}>Forgot Password ?</Text>
        </TouchableOpacity>

        <View style={{alignSelf:'center'}}>
        
          <TouchableOpacity
            onPress={()=>this.handleOnLogin()}
            style={{backgroundColor:'white',alignItems:'center',height:40,borderRadius:30,marginTop:15,width:320,justifyContent:'center'}}
            >
            <Text style={{color:'#55add5',fontSize:responsiveFontSize(2.5)}}>{this.state.loading?"Please Wait...":"Login"}</Text>
          </TouchableOpacity>
        </View>
              <View>
                <Text style={styles.errorText}>{this.state.callerror}</Text>
              </View>
        

        <Text style={{alignItems:'center',color:'white',alignSelf:'center',fontSize:responsiveFontSize(2)}}>OR</Text>

    <View style={{flexDirection:'row', justifyContent:'center', marginTop:30}}>
        <View style={{backgroundColor:'#1976d2', width:160, alignItems:'center',justifyContent:'center', borderTopLeftRadius:30, borderBottomLeftRadius:30, height:40}}>
          <TouchableOpacity style={{flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
              <Icon active name='facebook' type='FontAwesome' style={{color:'white', fontSize:15,marginTop:3}} />
              <Text style={{color:'white',fontSize:responsiveFontSize(2.2), marginLeft:8}}>Facebook</Text>
          </TouchableOpacity>
        </View>

        <View style={{backgroundColor:'white',justifyContent:'center', width:160, alignItems:'center', borderTopRightRadius:30, borderBottomRightRadius:30}}>
          <TouchableOpacity style={{flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
              <Icon active name='google' type='Zocial' style={{color:'red', fontSize:15,marginTop:3}} />
              <Text style={{color:'red',fontSize:responsiveFontSize(2.2), marginLeft:7}}>Google</Text>
          </TouchableOpacity>
        </View>
    </View>

      <Text style={{color:'white', alignSelf:'center', marginTop:20}}>Don't have an account?</Text>
      <TouchableOpacity 
      onPress={this.goToSignup}
      style={{width:80, alignSelf:'center'}}>
        <Text style={{color:'white', alignSelf:'center', fontSize:responsiveFontSize(1.7)}}>REGISTER</Text>
      </TouchableOpacity>
         
        </ScrollView>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  
  },
  logo: {
    width:"100%",
    height:"100%",
   // alignSelf:"center",
    //marginVertical:20,
    
  },
  // afterLogo : {
  //   paddingVertical:10,
  //   flex:1,
  // },
  feild : {
    width:responsiveWidth(90),
    alignSelf:"center",
    borderBottomColor:'#000',
    borderBottomWidth:0.5,
    fontSize:14
  },
  errorText: {
    color:"white",
    fontSize : 12,
    alignSelf:'center',
    marginTop:10
  },
  btnView:{
    width:responsiveWidth(90),
    alignSelf:"center",
    marginTop : 30,
  },
  usern:{
    marginTop:10,
    alignItems:'center',
    },
});

import React, { Component } from 'react';
import { StyleSheet, ImageBackground, ActivityIndicator, AsyncStorage, SafeAreaView,Text} from 'react-native';
import Firebase from "../../config/firebase/firebase"
import {responsiveWidth, responsiveHeight, responsiveFontSize} from 'react-native-responsive-dimensions';
class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  retrieveItem = async (key) => {
    return new Promise(async (resolve, reject) => {
        try {
            const retrievedItem = await AsyncStorage.getItem(key);
            const item = JSON.parse(retrievedItem);
            resolve(item);
        } catch (error) {
            reject(error.message);
        }
    })
  }

  componentDidMount = () => {
    setTimeout(()=>{
      this.retrieveItem("UID").then((response)=>{
        if(!response){
          this.props.navigation.navigate("auth");
        } else{
          this.props.navigation.navigate("tab");
        }
      }).catch((error) =>{
        alert(error)
      });
    },2000);
  } 
  
  render() {
    return (
      <SafeAreaView style={styles.container}>
      <ImageBackground
        source = {require("../../assets/background.jpg")}
        style={{flex:1}}
        >
        <ActivityIndicator
          style = {styles.loader} 
          size = {50}
          color="white"
           />
      </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  loader : {
    position:"absolute",
    bottom:57,
    alignSelf:'center'
  },
  container:{
    flex:1,
    justifyContent: 'center',
    alignContent: 'center'
},
txt:{
  fontSize: responsiveFontSize(4.5),
  alignSelf:'center',
  marginTop: responsiveHeight(85),
  color: 'white',
  fontWeight: 'bold', 
}

})

export default SplashScreen;
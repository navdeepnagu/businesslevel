import React, { PureComponent } from 'react';
import {  View, Text, ImageBackground,AsyncStorage } from 'react-native';
import { SafeAreaView } from "react-native-safe-area-context"
import Menu, { MenuItem, MenuDivider, Position } from "react-native-enhanced-popup-menu";
import {Icon} from "native-base"
import { TouchableOpacity } from "react-native-gesture-handler";
import {responsiveFontSize} from "react-native-responsive-dimensions"
import { render } from 'react-dom';
import UserProfile from "../UserProfile"
let textRef = React.createRef();
let menuRef = null;
 class ProfileScreen extends React.PureComponent{
   
  state={
    email:""
  }
  getdata=async()=>{
    var  user_email=await AsyncStorage.getItem('Email');
    const email = JSON.parse(user_email);
    this.setState({email:email});
   }
 render(){
this.getdata()
  const setMenuRef = ref => {menuRef = ref;}
  const hideMenu = () => {menuRef.hide();}
  const showMenu = () => {menuRef.show(textRef.current, stickTo = Position.TOP_LEFT);}
 
  const onPress = () => showMenu();

  const signout = () =>{
    AsyncStorage.removeItem("UID",()=>{
      this.props.navigation.navigate('auth');
    
    })
    hideMenu();
    
  }
 
  return (
    <SafeAreaView style={{flex:1}}>
    <View style={{ flex: 0.1, alignItems: "center", backgroundColor: "#55add5", flexDirection:"row"}}>
        <Text ref={textRef}> </Text>
        <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
          <Icon name="arrow-left" type="Feather" style={{color:'white', marginLeft:10, marginTop:5}}/>
        </TouchableOpacity>
        <Text style={{color:'white', fontSize:responsiveFontSize(2.9)}}> Profile </Text>
        <TouchableOpacity onPress={onPress}>
          <Icon name="dots-vertical" type="MaterialCommunityIcons" style={{color:'white', marginLeft:220}}/>
        </TouchableOpacity>
    </View>
             
      
      <Menu style={{marginLeft:255, marginTop:30}}
        ref={setMenuRef}>
            <MenuItem onPress={signout}>Sign Out</MenuItem>
        {/* <MenuItem onPress={hideMenu}>Item 2</MenuItem>
            <MenuItem onPress={hideMenu} disabled>Item 3</MenuItem>
            <MenuDivider />
            <MenuItem onPress={hideMenu}>Item 4</MenuItem> */}
      </Menu>
    <View style={{flex:0.9, backgroundColor:'black', alignItems:'center'}}>
      <UserProfile/>
          <Text style={{color:"white", marginTop:10}}>{this.state.email}</Text>
    </View>
   
    
    </SafeAreaView>
  );
          }

}
export default ProfileScreen;
import React, { PureComponent } from 'react';
import {  View, Text,ImageBackground,StyleSheet,TouchableOpacity,ScrollView, Image,TextInput, AsyncStorage, Alert } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {Icon, Card,CardItem,Item,Input} from "native-base"
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions"
import { Divider } from 'react-native-elements';
import Modal, { SlideAnimation,ModalContent } from 'react-native-modals';

class AddGarage extends PureComponent {
    arrayView=[]
    arrayContent=[
        {name:"Navdeep",phone:"9876543210",gname:"Bajaj Motors", gaddress:"abc"},
        {name:"Vicky",phone:"9874563210",gname:"Vicky Motors", gaddress:"def"},
        {name:"RK",phone:"9878541210",gname:"RK Motors", gaddress:"ghi"},
        {name:"Aman",phone:"9251343210",gname:"Aman Motors", gaddress:"jkl"},
      ]

    state={
        number:this.arrayContent.length,
        visible:false,
        passwordVisibility: true,
        pass:"",
        pass1:""
    }
    drawView=()=>{
      
        this.arrayView=[];
        for(let i=0;i<this.state.number;i++)
        {
            this.arrayView.push(

              <View style={{flex:1}}>
                <View style={{width:350,height:40,marginTop:10, backgroundColor:"#55add5", borderRadius:10,justifyContent:'center'}}>
                  <Text style={{color:'white', alignSelf:'center', fontSize:responsiveFontSize(2)}}>Garage</Text>
                </View>

                <View style={{width:350,height:150,backgroundColor:'white', borderRadius:10}}>
                  <Text style={[styles.txt,{marginTop:5}]}>Name : {this.arrayContent[i].name}</Text>
                  <Text style={styles.txt}>Phone : {this.arrayContent[i].phone}</Text>
                  <Text style={styles.txt}>Garage Name : {this.arrayContent[i].gname}</Text>
                  <Text style={styles.txt}>Garage Address : {this.arrayContent[i].gaddress}</Text>
                  <Divider style={{backgroundColor:'black', marginTop:5}}/>

                  <View style={{flexDirection:'row', justifyContent:'center', marginTop:10}}>

                    <View style={{backgroundColor:'#55add5', width:160,borderWidth:1, alignItems:'center',justifyContent:'center', borderTopLeftRadius:30, borderBottomLeftRadius:30, height:40}}>
                      <TouchableOpacity style={{flexDirection:'row', justifyContent:'center',alignItems:'center'}}
                        onPress={()=>{this.setState({visible:true})}}>
                          <Text style={{color:'white',fontSize:responsiveFontSize(2.2), marginLeft:8}}>Add Garage</Text>
                      </TouchableOpacity>
                    </View>

                    <View style={{backgroundColor:'white',justifyContent:'center',borderWidth:1, width:160, alignItems:'center', borderTopRightRadius:30, borderBottomRightRadius:30}}>
                      <TouchableOpacity style={{flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
                        <Text style={{color:'red',fontSize:responsiveFontSize(2.2), marginLeft:7}}>Decline</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            )
        }
    }

    getdata=async()=>{
      var  user_pass=await AsyncStorage.getItem('Password');
      const pass2 = JSON.parse(user_pass);
      this.setState({pass1:pass2});
     }
     onsubmit(){
       this.getdata();
       if(this.state.pass1==this.state.pass)
       {
         Alert.alert("New Garage Successfully Added !!!");
         this.setState({visible:false});
       }
       else{
         Alert.alert("Incorrect Password");
       }
       
     }

  render() {
  
    this.drawView();
    return (
      <SafeAreaView style={{flex:1}}>
       
        <View style={{flex:1, backgroundColor:'black', alignItems:"center"}}>
          <ScrollView showsVerticalScrollIndicator={false} style={{marginTop:0}}>
              {this.arrayView}
          </ScrollView>     
          <Modal
            visible={this.state.visible}
            onTouchOutside={() => {
            this.setState({ visible: false })}}
            modalAnimation={new SlideAnimation({
            slideFrom: 'bottom',
            })}>
              <ModalContent style={{width:300, height:150}}>
                <View style={{alignItems:'center', justifyContent:'center'}}>
                  <Text>Enter your password</Text>
                  <Item rounded style={{ width:250, height:40, marginTop:10}}>
                    <Icon active name='key' type='FontAwesome5' style={{color:'black', fontSize:17}} />
                    <Input 
                      onChangeText={(pass)=>this.setState({pass})}
                      secureTextEntry={this.state.passwordVisibility}
                      style={{color:"black"}}
                      placeholder='Password'
                    />
                  </Item>
                  <TouchableOpacity style={{borderWidth:1, marginTop:15, width:150, height:30,borderRadius:30,alignItems:'center',justifyContent:'center', backgroundColor:"#55add5"}}
                  onPress={this.onsubmit.bind(this)}>
                    <Text style={{color:'white'}}>Submit</Text>
                  </TouchableOpacity>
                </View>
              </ModalContent>
          </Modal>         
        </View>
      </SafeAreaView>
    );
  }
}
// AddGarage.navigationOptions={  
  
//   tabBarIcon:({tintColor, focused})=>(  
//   <Icon  
//       name={focused ? 'ios-home' : 'md-home'}  
//       color={tintColor}  
//       size={25}  
//   />  
// )  
// }  

export default AddGarage;

const styles=StyleSheet.create({
txt:{
  marginLeft:30,
  fontSize:responsiveFontSize(1.9)
}
});

import React, { PureComponent } from 'react';
import {  View, Text, TouchableOpacity, ImageBackground, Image, StyleSheet } from 'react-native';
import { SafeAreaView } from "react-native-safe-area-context";
import {Icon, Item, Card, CardItem, Body} from "native-base";
import {responsiveFontSize,responsiveHeight,responsiveWidth} from "react-native-responsive-dimensions"
import { ScrollView } from 'react-native-gesture-handler';

class HomeScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <SafeAreaView style={{flex:1}}>

          <View style={{flex:0.1, backgroundColor:'#55add5', alignItems:'center', flexDirection:'row'}}>
            <Icon name="home" type="FontAwesome" style={{color:'white', marginLeft:10, marginTop:5}}/>
            <Text style={{color:'white', fontSize:responsiveFontSize(2.9)}}> Home </Text>
          </View>

        <View style={{flex:0.9, backgroundColor:'black'}}>

          <Text style={styles.txt}>QUICK ACCESS</Text>
          <Item style={styles.items}>
            <View style={{flex:1,flexDirection:'row', justifyContent:'space-around'}}>
              <View style={{flexDirection:'column', alignItems:'center',marginLeft:10}}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("agarage")}>
                  <Image source={require("../../../assets/add_garage.png")}
                    style={{width:70,height:60}}
                  />
                </TouchableOpacity>
                <Text style={{color:'black'}}>Add Garage</Text>
              </View>

              <View style={{flexDirection:'column', alignItems:'center',marginLeft:10}}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("rgarage")}>
                  <Image source={require("../../../assets/remove_garage.png")}
                    style={{width:70,height:60}}
                  />
                </TouchableOpacity>
                <Text style={{color:'black'}}>Remove Garage</Text>
              </View>
            </View>
          </Item>

          <Text style={styles.txt}>LATEST UPDATES</Text>

        <ScrollView style={{marginTop:10}}>

            <Card style={styles.cmain}>
              <CardItem Header style={styles.chead}>
                <Text style={styles.ctext}>Garage Created!!!</Text>
              </CardItem>
              <CardItem style={styles.cbody}>
                <Body>
                  <Text style={styles.ctxt}>New Garage Created by ABC</Text>
                </Body>
                
              </CardItem>
            </Card>

            <Card style={styles.cmain}>
              <CardItem Header style={styles.chead}>
                <Text style={styles.ctext}>Garage Removed!!!</Text>
              </CardItem>
              <CardItem style={styles.cbody}>
                <Body>
                  <Text style={styles.ctxt}>Garage Removed by ABC</Text>
                </Body>
              </CardItem>
            </Card>

            <Card style={styles.cmain}>
              <CardItem Header style={styles.chead}>
                <Text style={styles.ctext}>Hello</Text>
              </CardItem>
              <CardItem style={styles.cbody}>
                <Body>
                  <Text style={styles.ctxt}>World</Text>
                </Body>
              </CardItem>
            </Card>

            <Card style={styles.cmain}>
              <CardItem Header style={styles.chead}>
                <Text style={styles.ctext}>Hello</Text>
              </CardItem>
              <CardItem style={styles.cbody}>
                <Body>
                  <Text style={styles.ctxt}>World</Text>
                </Body>
              </CardItem>
            </Card>

            <Card style={styles.cmain}>
              <CardItem Header style={styles.chead}>
                <Text style={styles.ctext}>Hello</Text>
              </CardItem>
              <CardItem style={styles.cbody}>
                <Body>
                  <Text style={styles.ctxt}>World</Text>
                </Body>
              </CardItem>
            </Card>
        </ScrollView>
      </View>
    </SafeAreaView>
    );
  }
}

export default HomeScreen;
const styles = StyleSheet.create({
    chead:{
      backgroundColor:'#55add5',
      borderBottomWidth:2,
      height:40,
      borderBottomColor:'#55add5', 
      borderTopWidth:2, 
      borderTopColor:'#55add5'
  },
  txt:{
    color:'white', 
    marginTop:20, 
    marginLeft:5, 
    fontSize:responsiveFontSize(2)
  },
  ctext:{
    color:"white",
    fontSize:responsiveFontSize(1.9),
    
  },
  cmain:{
    marginTop:5,
    borderBottomWidth:0.7
  },
  cbody:{
    height:100,
    backgroundColor:'white'
  },
  ctxt:{
    color:"black"
  },
  items:{
    backgroundColor:'white', 
    width:"100%",
    borderTopWidth:1,
    borderBottomWidth:1,
    borderColor:'#55add5', 
    height:"20%",
    flexDirection:'row', 
    alignItems:'center', 
    marginTop:5
  }
})
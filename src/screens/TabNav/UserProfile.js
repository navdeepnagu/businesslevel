import React from 'react';
import {StyleSheet,View } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { Avatar } from 'react-native-elements';


export default function UserProfile() {
    let [selectedImage, setSelectedImage] = React.useState(null);
  
    let openImagePickerAsync = async () => {
      let permissionResult = await ImagePicker.requestCameraRollPermissionsAsync();
  
      if (permissionResult.granted === false) {
        alert('Permission to access camera roll is required!');
        return;
      }
  
      let pickerResult = await ImagePicker.launchImageLibraryAsync();
  
      if (pickerResult.cancelled === true) {
        return;
      }
  
      setSelectedImage({ localUri: pickerResult.uri });
    };
  
    if (selectedImage !== null) {
      return (
        <View style={styles.container}>
        <Avatar
        source={{ uri: selectedImage.localUri }}
        rounded
        size="large"
        showEditButton
        onPress={openImagePickerAsync}/>
  
        
      </View>
      );
    }
  
    return (
        <View style={styles.container}>
        <Avatar
        source={require('../../assets/profile1.png')} 
        rounded
        size="large"
        containerStyle={{backgroundColor:"black"}}
        showEditButton
        onPress={openImagePickerAsync}/>
        
        
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    container:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'black',
        marginTop:40,
       
    }
  });
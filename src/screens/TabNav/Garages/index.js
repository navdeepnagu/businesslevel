import React, { PureComponent } from 'react';
import {  View, Text, ImageBackground, StyleSheet, StatusBar } from 'react-native';
import { SafeAreaView } from "react-native-safe-area-context"
import {Icon} from "native-base"
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {createAppContainer} from 'react-navigation';  
import AppNavigator from '../../../navigators/TopTabNav/index';  
const AppIndex = createAppContainer(AppNavigator)  

class Garages extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <SafeAreaView style={{flex:1}}>
        
        <StatusBar barStyle = "light-content" hidden = {false} backgroundColor = "#1c739a" translucent = {true}/>

        <View style={{flex:0.1, backgroundColor:'#55add5', flexDirection:'row', alignItems:'center', justifyContent:"center"}}>
          <Text style={{color:'white',fontSize:responsiveFontSize(2.9)}}> Garages </Text>
        </View>

        <View style={{flex:0.9, backgroundColor:'black'}}>
         <AppIndex/>
        </View>

      </SafeAreaView>
    );
  }
}

export default Garages;
const styles = StyleSheet.create({
 
txt:{
  color:'white', 
  marginTop:20, 
  marginLeft:5, 
  fontSize:responsiveFontSize(2)
},
})
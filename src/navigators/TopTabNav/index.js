import React from 'react';  
import {createAppContainer} from 'react-navigation'; 
import {createMaterialTopTabNavigator} from "react-navigation-tabs"; 
import AddGarage from "../../screens/TabNav/AddGarage/index";  
import RemoveGarage from "../../screens/TabNav/RemoveGarage/index";  
import ViewGarage from "../../screens/TabNav/ViewGarages/index" 
  
const AppNavigator = createMaterialTopTabNavigator(  
    {  
        Add: AddGarage,  
        Remove: RemoveGarage,  
        View: ViewGarage
    },  
    {  
        tabBarOptions: {  
            activeTintColor: '#55add5',
            inactiveTintColor:"white",  
            showIcon: false,
            showLabel:true,
            indicatorStyle:{backgroundColor:"#55add5"},
            labelStyle:{fontSize:15},
            style: {  
                backgroundColor:'black',height:50,
            }  
        },  
    }  
)  
export default createAppContainer(AppNavigator); 
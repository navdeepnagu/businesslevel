import {createAppContainer} from "react-navigation";
import {createBottomTabNavigator} from 'react-navigation-tabs'
import React from 'react';
import {Icon} from 'native-base'
import ProfileScreen from "../../screens/TabNav/ProfileScreen/index"
import Garages from "../../screens/TabNav/Garages/index"
const TabNav= createBottomTabNavigator({

Garages:{
    screen : Garages,
    navigationOptions: {
        title:'Garages',            
        tabBarIcon:({ tintColor }) =>{
            return(
                <Icon name='tool'
                      type='AntDesign'
                      style={{
                          color:tintColor,
                          fontSize : 20
                      }} />
                      )
                    }
  }
},
ProfileScreen:{
    screen : ProfileScreen,
    navigationOptions: {
        title:'Profile',            
        tabBarIcon:({ tintColor }) =>{
            return(
                <Icon name='user'
                      type='FontAwesome5'
                      style={{
                          color:tintColor,
                          fontSize : 20,
                      }} />
                      )
                    }
  }
},
},

        {
        initialRouteName:"Garages",
        tabBarOptions:{
            activeTintColor:'#fff',
            inactiveTintColor:'#fff',
            inactiveBackgroundColor:'black',
            activeBackgroundColor:'#55add5'
        }
        
});
export default createAppContainer(TabNav);
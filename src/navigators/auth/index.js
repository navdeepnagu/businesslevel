import {createStackNavigator} from "react-navigation-stack";
import {createAppContainer} from "react-navigation";

import {LoginScreen, SignupScreen, ForgotPassword} from "../../screens/auth"

const AuthNavigator=createStackNavigator({
    login:{
        screen:LoginScreen
    },
    signup:{
        screen:SignupScreen
    },
    forgotpassword:{
        screen:ForgotPassword
    }
},{
    initialRouteName:"login",
    headerMode:null
});
export default createAppContainer(AuthNavigator);
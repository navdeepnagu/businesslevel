import {createStackNavigator} from "react-navigation-stack";
import {createAppContainer} from "react-navigation";

import TabNav from "./TabNav"
import AGarage from "../screens/TabNav/AddGarage/index"
import RGarage from "../screens/TabNav/RemoveGarage/index"
import Auth from "./auth"
import Splash from "../screens/splash"


const AppNavigator = createStackNavigator({
    tab : {
        screen :TabNav
    },
    agarage:{
        screen : AGarage
    },
    rgarage:{
        screen : RGarage
    },
    auth:{
        screen : Auth
    },
    splash:{
        screen : Splash
    }
    
},{
    initialRouteName:"splash",
    headerMode:null
});
export default createAppContainer(AppNavigator);